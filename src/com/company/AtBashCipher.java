package com.company;

class AtBashCipher implements Cipher {

    private final String encodingArr = "abcdefghijklm nopqrstuvwxyz";

    @Override
    public String decode(String message) {
        return encode(message);
    }

    @Override
    public String encode(String message) {

        StringBuilder encodedText = new StringBuilder();
        message = message.toLowerCase();
        for (int i = 0; i < message.length(); i++) {

            char currentChar = message.charAt(i);
            int pos = encodingArr.indexOf(currentChar);
            char change = encodingArr.charAt(encodingArr.length() - pos - 1);
            encodedText.append(change);
        }

        return encodedText.toString();
    }
}
